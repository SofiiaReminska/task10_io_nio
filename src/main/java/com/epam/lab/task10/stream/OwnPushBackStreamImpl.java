package com.epam.lab.task10.stream;

import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;

public class OwnPushBackStreamImpl extends InputStream {
    private ArrayDeque<Byte> buf;
    private InputStream in;

    public OwnPushBackStreamImpl(InputStream in) {
        this.in = in;
        this.buf = new ArrayDeque<>();
    }

    @Override
    public int read() throws IOException {
        ensureOpen();
        if (!buf.isEmpty()) {
            return buf.pop();
        }
        return in.read();
    }

    public int read(byte[] b, int off, int len) throws IOException {
        ensureOpen();
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        int avail = buf.size();
        if (avail > 0) {
            if (len < avail) {
                avail = len;
            }
            System.arraycopy(ArrayUtils.toPrimitive(buf.toArray(new Byte[buf.size()])), 0, b, off, avail);
            for (int i = 0; i < avail; i++) {
                buf.pop();
            }
            off += avail;
            len -= avail;
        }
        if (len > 0) {
            len = super.read(b, off, len);
            if (len == -1) {
                return avail == 0 ? -1 : avail;
            }
            return avail + len;
        }
        return avail;
    }

    private void ensureOpen() throws IOException {
        if (in == null)
            throw new IOException("Stream closed");
    }

    public void unread(int b) throws IOException {
        ensureOpen();
        buf.push((byte) b);
    }

    public void unread(byte[] b) throws IOException {
        for (int i = b.length - 1; i >= 0; i--) {
            buf.push(b[i]);
        }
    }
}
