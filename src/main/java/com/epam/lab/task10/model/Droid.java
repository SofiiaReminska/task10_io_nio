package com.epam.lab.task10.model;

import java.io.Serializable;
import java.util.Random;

public class Droid implements Serializable {
    private int healthPoints = 50;
    private int power = 9;
    private transient int id;

    public Droid(int healthPoints, int power){
        this.healthPoints = healthPoints;
        this.power = power;
        this.id = new Random().nextInt();
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "healthPoints=" + healthPoints +
                ", power=" + power +
                ", id=" + id +
                '}';
    }
}
