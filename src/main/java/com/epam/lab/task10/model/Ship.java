package com.epam.lab.task10.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Ship<T extends Droid> implements Serializable {
    private List<T> droids;
    public Ship() {
        droids = new LinkedList<T>();
    }

    public List<T> getDroids() {
        return droids;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "droids=" + droids +
                '}';
    }
}
