package com.epam.lab.task10.task;

import com.epam.lab.task10.stream.OwnPushBackStreamImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class OwnPushBackImplDemo {
    private static final Logger LOGGER = LogManager.getLogger(OwnPushBackImplDemo.class);

    public static void main(String[] args) {
        String fileName = "ship.txt";
        StringBuilder sb = new StringBuilder();
        try (OwnPushBackStreamImpl inputStream = new OwnPushBackStreamImpl(new FileInputStream(fileName))) {
            int c;
            while ((c = inputStream.read()) != -1) {
                sb.append(((char) c));
            }
            LOGGER.info(sb.toString());
            inputStream.unread('A');
            inputStream.unread('U');
            inputStream.unread('E');
            final byte[] b = new byte[2];
            LOGGER.info(inputStream.read(b, 0, 2));
            LOGGER.info(Arrays.toString(b));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
