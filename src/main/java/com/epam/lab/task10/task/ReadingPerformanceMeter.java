package com.epam.lab.task10.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadingPerformanceMeter {
    private static final Logger LOGGER = LogManager.getLogger(ReadingPerformanceMeter.class);

    public static void main(String[] args) throws IOException {
        String fileName = "test_file.pdf";
        long start;
        long end;
        try (FileInputStream inputStream = new FileInputStream(fileName)) {
            int c;
            start = System.currentTimeMillis();
            while ((c = inputStream.read()) != -1) {
                c = inputStream.read();
            }
            end = System.currentTimeMillis();
            LOGGER.info("(FileInputStream) read file...{} seconds", (float) (end - start) / 1000);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(fileName))) {
            int c;
            start = System.currentTimeMillis();
            while ((c = bufferedInputStream.read()) != -1) {
                c = bufferedInputStream.read();
            }
            end = System.currentTimeMillis();
            LOGGER.info("(BufferedInputStream) read file...{} seconds", (float) (end - start) / 1000);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
