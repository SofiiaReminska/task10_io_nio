package com.epam.lab.task10.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FolderExplorer {
    private static final Logger LOGGER = LogManager.getLogger(FolderExplorer.class);

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter path");
        while (sc.hasNext()) {
            String path = sc.nextLine();
            LOGGER.info(retrieveFilesAndDirectories(path));
            LOGGER.info("Enter path");
        }
    }

    private static String retrieveFilesAndDirectories(String path) throws IOException {
        Path file1 = Paths.get(path);
        StringBuilder sb = new StringBuilder();
        final List<Path> collect = Files.list(file1).collect(Collectors.toList());
        for (Path p : collect) {
            sb.append(retrieveFileAttributes(p));
        }
        return sb.toString();
    }

    private static String retrieveFileAttributes(Path p) throws IOException {
        BasicFileAttributes basicFileAttributes = Files.readAttributes(p, BasicFileAttributes.class);
        return String.format("%-20s %20s %20s %20s%n",
                p.getFileName(),
                basicFileAttributes.creationTime(),
                Files.isRegularFile(p) ? "FILE\t" : "DIR\t",
                basicFileAttributes.size());
    }
}
