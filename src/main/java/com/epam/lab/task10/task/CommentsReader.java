package com.epam.lab.task10.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.NoSuchFileException;
import java.util.Scanner;

public class CommentsReader {
    private static final Logger LOGGER = LogManager.getLogger(CommentsReader.class);
    private static final String END_COMMENT = "*/";
    private static final String START_COMMENT = "/*";
    private static final String COMMENT = "//";

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String path = sc.nextLine();
        LOGGER.info(parseCommentsFromFile(path));
    }

    private static String parseCommentsFromFile(String path) throws IOException {
        StringBuilder sb;
        try (BufferedReader reader = getBufferedReaderForPath(path)) {
            String s;
            sb = new StringBuilder();
            boolean multiLineComment = false;
            while ((s = reader.readLine()) != null) {
                if (multiLineComment) {
                    if (s.contains(END_COMMENT)) {
                        multiLineComment = false;
                        s = s.substring(0, s.indexOf(END_COMMENT) + 2);
                    }
                    sb.append(s).append('\n');
                } else if (s.contains(COMMENT)) {
                    s = s.substring(s.indexOf(COMMENT));
                    sb.append(s).append('\n');
                } else if (s.contains(START_COMMENT)) {
                    s = s.substring(s.indexOf(START_COMMENT));
                    sb.append(s).append('\n');
                    multiLineComment = true;
                }
            }
        }
        return sb.toString();
    }

    private static BufferedReader getBufferedReaderForPath(String path) throws NoSuchFileException, FileNotFoundException {
        final File file = new File(path);
        if (!file.exists()) {
            throw new NoSuchFileException("Can`t find such file");
        }
        return new BufferedReader(new FileReader(file));
    }
}
