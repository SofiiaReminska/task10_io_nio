package com.epam.lab.task10.task;

import com.epam.lab.task10.model.Droid;
import com.epam.lab.task10.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Serializator {
    private static final Logger LOGGER = LogManager.getLogger(Serializator.class);

    public static void main(String[] args) throws FileNotFoundException {

        Ship<Droid> ship = new Ship<>();
        ship.getDroids().add(new Droid(45, 10));
        ship.getDroids().add(new Droid(35, 9));
        String filename = "ship.txt";
        LOGGER.info("Before serialization: {}", ship.getDroids());
        try {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(ship);
            out.close();
            file.close();
            LOGGER.info("Object has been serialized");
        } catch (IOException ex) {
            LOGGER.error("IOException is caught");
        }
        Ship<Droid> ship1 = null;
        try {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);
            ship1 = (Ship<Droid>) in.readObject();
            in.close();
            file.close();
            LOGGER.info("Object has been deserialized ");
            LOGGER.info("Ship of droids: {}", ship1.getDroids());
        } catch (IOException ex) {
            LOGGER.error("IOException is caught");
        } catch (ClassNotFoundException ex) {
            LOGGER.error("ClassNotFoundException is caught");
        }
    }
}
